package com.roaringcatgames.ludumdare.util;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 12/6/14
 * Time: 11:12 AM
 * To change this template use File | Settings | File Templates.
 */
public class MathUtil {

    public static double getSineYForTime(double period, double scale, double timePosition){
        return  Math.sin(timePosition*2*Math.PI/period)*(scale/2) + (scale/2);
    }
}
