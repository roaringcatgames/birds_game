package com.roaringcatgames.ludumdare.util;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 12/6/14
 * Time: 9:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class AssetUtil {
    public static Class<BitmapFont> BITMAP_FONT = BitmapFont.class;
    public static Class<Texture> TEXTURE = Texture.class;
    public static Class<Sound> SOUND = Sound.class;
    public static Class<Music> MUSIC = Music.class;
    public static Class<TextureAtlas> TEXTURE_ATLAS = TextureAtlas.class;
    public static Class<Skin> SKIN = Skin.class;

    public static String ANI_ATLAS = "animations/animations.atlas";

    public static String SFX_TRANSFORMER = "sfx/transformer.mp3";
    //public static String SFX_BIRD_FLAP_SLOW = "sfx/bird_flap_slow.mp3";
    public static String SFX_BIRD_FLAP_FAST = "sfx/bird_flap_fast.mp3";
    public static String SFX_BIRD_FLAP_DEEP = "sfx/bird_flap_deep.mp3";
    public static String SFX_BIRD_SCREAM = "sfx/bird_scream.mp3";
    //public static String SFX_BIRD_COOING = "sfx/light-cooing.mp3";
    public static String SFX_BIRD_FLUTTER = "sfx/wing-flutter.mp3";
    public static String MSC_AMBIENT_BIRDS = "sfx/ambient-birds.mp3";
    public static String MSC_BG_MUSIC = "sfx/dt3.mp3";

    public static String SCARE_GLOW = "images/Green-Glow.png";
    public static String BG_SKY = "bg/Rotating_BG.jpg";
    public static String BG_SKY_A = "bg/BG_A.jpg";
    public static String BG_SKY_B = "bg/BG_B.jpg";
    public static String BG_SKY_C = "bg/BG_C.jpg";
    public static String BG_SKY_D = "bg/BG_D.jpg";

    public static String BG_BASE = "bg/Background.png";
    public static String BG_POLE = "bg/Pole.png";
    public static String BG_LINE_LT = "bg/String_Left_Top.png";
    public static String BG_LINE_LM = "bg/String_Left_Mid.png";
    public static String BG_LINE_LB = "bg/String_Left_Low.png";

    public static String BG_LINE_RT = "bg/String_Right_Top.png";
    public static String BG_LINE_RM = "bg/String_Right_Mid.png";
    public static String BG_LINE_RB = "bg/String_Right_Low.png";
//    public static String LIFE_ICON =  "images/life-indicator.png";
//    public static String LIFE_ICON_USED = "images/life-indicator.png";
    //public static String LIFE_ICON =  "images/Icon_White.png";
//    public static String LIFE_ICON_USED = "images/Icon_Red.png";
    public static String LIFE_ICON =  "images/Icon_Glow.png";
    public static String LIFE_ICON_USED = "images/Icon_Glow.png";

    //Font Paths
    //public static final String REXLIA_24 = "font/rexlia-24.fnt";
    public static final String REXLIA_16 = "font/rexlia-16.fnt";


}
