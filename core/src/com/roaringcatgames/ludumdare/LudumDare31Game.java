package com.roaringcatgames.ludumdare;

import com.badlogic.gdx.*;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.kasetagen.engine.IDataSaver;
import com.kasetagen.engine.IGameProcessor;
import com.kasetagen.engine.gdx.scenes.scene2d.KasetagenStateUtil;
import com.kasetagen.engine.screen.LoadingScreen;
import com.roaringcatgames.ludumdare.screen.GameScreen;
import com.roaringcatgames.ludumdare.util.AssetUtil;

public class LudumDare31Game extends Game implements IGameProcessor{

    public static final String LOADING = "loading";
    public static final String START = "STARTSCREEN";
    public static final String DATA_NAME = "BirdsOnLinesData";

    private InputMultiplexer inputChain;
    private LoadingScreen loading;
    private GameScreen birdScreen;
    private boolean isControllerEnabled = false;
    private boolean isInitialized = false;

    protected AssetManager assetManager;




    public LudumDare31Game(boolean isControllerEnabled){
        this.isControllerEnabled = isControllerEnabled;
        this.inputChain = new InputMultiplexer();
    }

    @Override
    public void create () {

        if(isControllerEnabled){
            Graphics.DisplayMode dm = Gdx.graphics.getDesktopDisplayMode();
            Gdx.app.log("DISPLAY", " Using Controller Mode. W: " + dm.width + " H: " + dm.height + " X: " + dm.bitsPerPixel);
            Gdx.graphics.setDisplayMode(dm.width, dm.height, true);
            Gdx.graphics.setVSync(true);
            Gdx.input.setCursorCatched(true);
        }
        assetManager = new AssetManager();
        loadAssets();
        Gdx.input.setCursorPosition(ViewportUtil.VP_WIDTH/2, ViewportUtil.VP_HEIGHT/2);
        Gdx.input.setCursorCatched(true);

    }

    @Override
    public void render () {

        if(assetManager.update()){
            if(!isInitialized){
                KasetagenStateUtil.setDebugMode(false);
                Pixmap pm = new Pixmap(Gdx.files.internal("transparent.png"));
                Gdx.input.setCursorImage(pm, pm.getWidth()/2, pm.getHeight()/2);
                //pm.dispose();
                changeToScreen(START);
                isInitialized = true;
            }
        }else{
            if(!isInitialized){
                changeToScreen(LOADING);
                isInitialized = true;
            }
        }

        Gdx.gl.glClearColor(.16f, .14f, .13f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        super.render();

    }

    public void loadAssets(){
//        assetManager.load(AssetUtil.REXLIA_64, AssetUtil.BITMAP_FONT);
//        assetManager.load(AssetUtil.REXLIA_48, AssetUtil.BITMAP_FONT);
//        assetManager.load(AssetUtil.REXLIA_32, AssetUtil.BITMAP_FONT);
//        assetManager.load(AssetUtil.REXLIA_24, AssetUtil.BITMAP_FONT);
        assetManager.load(AssetUtil.REXLIA_16, AssetUtil.BITMAP_FONT);

        assetManager.load(AssetUtil.ANI_ATLAS, AssetUtil.TEXTURE_ATLAS);
        assetManager.load(AssetUtil.MSC_AMBIENT_BIRDS, AssetUtil.MUSIC);
        assetManager.load(AssetUtil.MSC_BG_MUSIC, AssetUtil.MUSIC);
        assetManager.load(AssetUtil.SFX_TRANSFORMER, AssetUtil.SOUND);
        //assetManager.load(AssetUtil.SFX_BIRD_FLAP_SLOW, AssetUtil.SOUND);
        assetManager.load(AssetUtil.SFX_BIRD_FLAP_FAST, AssetUtil.SOUND);
        assetManager.load(AssetUtil.SFX_BIRD_FLAP_DEEP, AssetUtil.SOUND);
        assetManager.load(AssetUtil.SFX_BIRD_SCREAM, AssetUtil.SOUND);
        //assetManager.load(AssetUtil.SFX_BIRD_COOING, AssetUtil.SOUND);
        assetManager.load(AssetUtil.SFX_BIRD_FLUTTER, AssetUtil.SOUND);

        assetManager.load(AssetUtil.SCARE_GLOW, AssetUtil.TEXTURE);
        //assetManager.load(AssetUtil.BG_SKY, AssetUtil.TEXTURE);
        assetManager.load(AssetUtil.BG_SKY_A, AssetUtil.TEXTURE);
        assetManager.load(AssetUtil.BG_SKY_B, AssetUtil.TEXTURE);
        assetManager.load(AssetUtil.BG_SKY_C, AssetUtil.TEXTURE);
        assetManager.load(AssetUtil.BG_SKY_D, AssetUtil.TEXTURE);

        assetManager.load(AssetUtil.BG_BASE, AssetUtil.TEXTURE);
        assetManager.load(AssetUtil.BG_POLE, AssetUtil.TEXTURE);
        assetManager.load(AssetUtil.BG_LINE_LT, AssetUtil.TEXTURE);
        assetManager.load(AssetUtil.BG_LINE_LM, AssetUtil.TEXTURE);
        assetManager.load(AssetUtil.BG_LINE_LB, AssetUtil.TEXTURE);

        assetManager.load(AssetUtil.BG_LINE_RT, AssetUtil.TEXTURE);
        assetManager.load(AssetUtil.BG_LINE_RM, AssetUtil.TEXTURE);
        assetManager.load(AssetUtil.BG_LINE_RB, AssetUtil.TEXTURE);
        assetManager.load(AssetUtil.LIFE_ICON, AssetUtil.TEXTURE);
        assetManager.load(AssetUtil.LIFE_ICON_USED, AssetUtil.TEXTURE);
    }

    @Override
    public String getStartScreen() {
        return START;
    }

    @Override
    public boolean isLoaded() {
        return assetManager.update();
    }

    @Override
    public float getLoadingProgress() {
        return assetManager.getProgress();
    }

    @Override
    public AssetManager getAssetManager() {
        return assetManager;
    }

    @Override
    public void changeToScreen(String screenName) {
        if(LOADING.equalsIgnoreCase(screenName))    {
            if(loading == null){
                loading = new LoadingScreen(this, "animations/loading.atlas",
                          new Stage(new FitViewport(ViewportUtil.VP_WIDTH, ViewportUtil.VP_HEIGHT)));
            }

            setScreen(loading);
        }else if(START.equalsIgnoreCase(screenName)){
            if(birdScreen == null){
                birdScreen = new GameScreen(this);
            }

            setScreen(birdScreen);
            inputChain.clear();
            inputChain.addProcessor(birdScreen);
            inputChain.addProcessor(birdScreen.getStage());
            Gdx.input.setInputProcessor(inputChain);
        }
    }

    @Override
    public String getStoredString(String key) {
        Preferences preferences = Gdx.app.getPreferences(DATA_NAME);
        String value = "";
        if(preferences.contains(key)){
            value = preferences.getString(key);
        }
        return value;
    }

    @Override
    public String getStoredString(String key, String defaultValue) {
        String value = getStoredString(key);
        if(value == null || "".equals(value.trim())){
            value = defaultValue;
        }
        return value;
    }

    @Override
    public int getStoredInt(String key) {
        Preferences preferences = Gdx.app.getPreferences(DATA_NAME);
        int value = -1;
        if(preferences.contains(key)){
            value = preferences.getInteger(key);
        }
        return value;
    }

    @Override
    public float getStoredFloat(String key) {
        Preferences preferences = Gdx.app.getPreferences(DATA_NAME);
        float value = -1f;
        if(preferences.contains(key)){
            value = preferences.getFloat(key);
        }
        return value;
    }

    @Override
    public void saveGameData(IDataSaver saver) {
        Preferences preferences = Gdx.app.getPreferences(DATA_NAME);
        saver.updatePreferences(preferences);
        preferences.flush();
    }
}
