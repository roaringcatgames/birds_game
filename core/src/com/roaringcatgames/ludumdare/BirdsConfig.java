package com.roaringcatgames.ludumdare;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 12/23/14
 * Time: 7:49 PM
 *
 * This class will be used to send into the Game class to initialize the game settings. We'll enable switches
 * that can be used to toggle features on/off, and help us implement the LiveWallpaper.
 */

public class BirdsConfig {

    public boolean isSoundEnabled = true;
    public boolean isShockModeEnabled = true;
    public boolean trackTouches = true;
    public int maximumBirdCount = 1000;


}
