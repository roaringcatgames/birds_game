package com.roaringcatgames.ludumdare.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kasetagen.engine.IGameProcessor;
import com.kasetagen.engine.screen.Kitten2dScreen;
import com.roaringcatgames.ludumdare.scene2d.MainStage;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 12/6/14
 * Time: 9:29 AM
 * To change this template use File | Settings | File Templates.
 */
public class GameScreen extends Kitten2dScreen{

    public GameScreen(IGameProcessor delegate) {
        super(delegate);
        this.stage = new MainStage(delegate);
        this.enableDebugRender = false;
    }

    @Override
    public void render(float delta) {
        if(Gdx.graphics.isFullscreen()){
            Viewport vp = stage.getViewport();
            int screenW = vp.getScreenWidth();
            int screenH = vp.getScreenHeight();
            int leftCrop = vp.getLeftGutterWidth();
            int bottomCrop = vp.getBottomGutterHeight();
            int xPos = leftCrop;
            int yPos = bottomCrop;

            Gdx.gl.glViewport(xPos, yPos, screenW, screenH);
            Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        }
        super.render(delta);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
