package com.roaringcatgames.ludumdare.scene2d.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;
import com.roaringcatgames.ludumdare.util.MathUtil;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 12/6/14
 * Time: 10:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class ScareCrow extends GenericActor{
    private double OSCILATING_PERIOD = 4;

    private float oscillatedScale;
    private double elapsedTime = 0f;
    private float baseDimension = 1f;

    public ScareCrow(float x, float y, float width, float height, TextureRegion region, float oscillatedScale) {
        super(x, y, width, height, region, Color.BLACK);
        this.oscillatedScale = oscillatedScale;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        elapsedTime += delta;
        double w = 1.25f;

        oscillatedScale = (float)MathUtil.getSineYForTime(OSCILATING_PERIOD, w, elapsedTime) + baseDimension;
    }

    @Override
    protected void drawFull(Batch batch, float parentAlpha) {
        if(textureRegion != null){
            Color origColor = batch.getColor();
            batch.setColor(1f, 1f, 1f, 0.2f);
            batch.draw(textureRegion, getX(), getY(), getOriginX(), getOriginY(),
                    getWidth(), getHeight(), oscillatedScale, oscillatedScale, getRotation());
            batch.setColor(origColor);
        }
    }
}
