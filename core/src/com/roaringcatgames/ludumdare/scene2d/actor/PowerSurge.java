

package com.roaringcatgames.ludumdare.scene2d.actor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.AnimatedActor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericGroup;

/**
 * Created by rexsoriano on 12/6/14.
 */
public class PowerSurge extends AnimatedActor{
    private Vector2 targetPosition;
    public int lineIndex;
    public boolean isWarning = true;
    public PowerSurge(float x, float y, float width, float height,
                Animation animation, Vector2 targetPosition ) {
        super(x, y, width, height, animation, 0f);
        this.targetPosition = targetPosition;
    }

    public Vector2 getTargetPosition() {return targetPosition;}
    
}
