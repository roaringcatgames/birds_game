package com.roaringcatgames.ludumdare.scene2d.actor;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.AnimatedActor;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 12/6/14
 * Time: 5:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class Bird extends AnimatedActor{

    private static final float THRESHHOLD = 250f;
    private Vector2 targetPosition;
    public boolean isLeftFacing = false;
    public boolean isLeaving = false;
    public boolean isShocked = false;
    public boolean isFlapping = false;
    public int lineIndex;
    public Bird(float x, float y, float width, float height,
                Animation animation, Vector2 targetPosition) {
        super(x, y, width, height, animation, 0f);
        this.targetPosition = targetPosition;
    }

    public Vector2 getTargetPosition(){
        return targetPosition;
    }

    public boolean isOutOfRange(){
        String status = getCurrentState();
        float distance = Math.abs(Vector2.dst(getX(), getY(), targetPosition.x, targetPosition.y));
        return !"DEFAULT".equals(status) && distance > THRESHHOLD;
    }

    public void setTargetPosition(Vector2 pos){
        targetPosition = pos;
    }

    @Override
    protected void drawFull(Batch batch, float parentAlpha) {

        String status = getCurrentState();
        if("DEFAULT".equals(status) || "FLYING".equals(status)){
            if(textureRegion != null){
                batch.draw(textureRegion, getX(), getY(), getOriginX(), getOriginY(),
                        getWidth(), getHeight(), 1.25f, 1.25f, getRotation());
            }
        }else{
            super.drawFull(batch, parentAlpha);
        }
    }
}
