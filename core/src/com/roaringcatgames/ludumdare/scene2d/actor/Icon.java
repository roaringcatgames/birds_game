package com.roaringcatgames.ludumdare.scene2d.actor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 12/7/14
 * Time: 4:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class Icon  extends GenericActor{

    private TextureRegion deadRegion;

    public Icon(float x, float y, float width, float height, TextureRegion textureRegion, Color color) {
        super(x, y, width, height, textureRegion, color);
    }

    public void setDeadRegion(TextureRegion tr){
        deadRegion = tr;
    }

    public void markUsed(){
        textureRegion = deadRegion;
    }

    @Override
    protected void drawFull(Batch batch, float parentAlpha) {
        Color c = batch.getColor();
        if(textureRegion.isFlipX()){
            batch.setColor(1f, 0f, 0f, 0.5f);
        }
        super.drawFull(batch, parentAlpha);
        batch.setColor(c);

    }
}
