package com.roaringcatgames.ludumdare.scene2d;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.kasetagen.engine.IGameProcessor;
import com.kasetagen.engine.gdx.scenes.scene2d.ActorDecorator;
import com.kasetagen.engine.gdx.scenes.scene2d.IActorDisposer;
import com.kasetagen.engine.gdx.scenes.scene2d.Kitten2dStage;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.AnimatedActor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericGroup;
import com.roaringcatgames.ludumdare.ViewportUtil;
import com.roaringcatgames.ludumdare.scene2d.actor.Bird;
import com.roaringcatgames.ludumdare.scene2d.actor.Icon;
import com.roaringcatgames.ludumdare.scene2d.actor.PowerSurge;
import com.roaringcatgames.ludumdare.scene2d.actor.ScareCrow;
import com.roaringcatgames.ludumdare.util.AssetUtil;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 12/6/14
 * Time: 9:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class MainStage extends Kitten2dStage {

    private static final Random rand = new Random(System.currentTimeMillis());

    private String GM_CHILL = "CHILL";
    private String GM_SHOCK = "SHOCK";

    private float SIT_CYCLE_RATE = 1f/3f;
    private float FLY_CYCLE_RATE = 1f/6f;
    private float SHOCK_CYCLE_RATE = 1f/6f;
    private float COOKED_CYCLE_RATE = 1f/3f;
    private float BASE_SKY_ROTATION = 0f;
    private float CLOUD_ROTATION_RATE = 0.4f;
    private float TREE_CYCLE_RATE = 1f/4f;
    private float CTRL_BOX_CYCLE_RATE = 1f/10f;

    private float BIRD_SPAWN_INTERVAL = 2f;
    private float SURGE_SPAWN_INTERVAL = 5f;
    private float LIFE_SPAWN_INTERVAL = 10f;
    private float MAX_SPAWN_Y = 1500f;
    private float MIN_SPAWN_Y = 400f;
    private float LEFT_SPAWN_X = -100f;
    private float RIGHT_SPAWN_X = 1320f;
    private float BIRD_SIZE = 50f;
    private float BIRD_FLY_IN_TIME = 1f;
    private int BIRD_LIVES = 3;
    private final float POLE_CENTER_X = 822f;
    private final float POLE_CENTER_Y = 600f;
    private final float PLANE_WIDTH = 500f;
    private final float PLANE_HEIGHT = 200f;
    private final float CTRL_BOX_SIZE = 150f;
    private float SCARECROW_SIZE;

    private final float ICON_SIZE = 30f;

    private float finalFlockSize = 0f;

    ScareCrow scareCrow;
    Array<Bird> birds;
    IActorDisposer birdDisposer;
    Array<PowerSurge> surges;
    Array<Icon> birdLifeIcons;
    IActorDisposer surgeDisposer;
    private float birdSpawnTimeElapsed = 0f;
    private float surgeSpawnTimeElapsed = 0f;
    private int lifeSpawn = 0;
    private int deadBirds = 0;


    private GenericGroup background;
    private GenericGroup hugeAssSky;
    private AnimatedActor controlBox;
    private AnimatedActor boxSpark;
    private Array<Animation> birdSitAnimations;
    private Array<Animation> birdFlyAnimations;
    private Array<Animation> shockAnimations;
    private Array<Animation> cookedAnimations;

    private static String[] lineNames = new String[] {"LINE-A", "LINE-B", "LINE-C", "LINE-D", "LINE-E", "LINE-F"};
    private ObjectMap<String, Vector2[]> lineMap;

    private boolean hasMoved = false;
    private Vector2 lastPosition;
    private Vector2 scarecrowVelocity;

    private boolean loseCondition = false;
    private boolean gameOver = false;

    private String gameMode = GM_CHILL;

    //HUD
    private Label birdCountLabel;

    //Ambience
    private Sound fastFlap;
    private Sound deepFlap;
    private Sound scream;
    private Sound flutter;
    private Music ambientBirds;
    private Music bgMusic;
    private Array<Sound> flapSounds;

    //Processing Loop Temp Objects
    private Array<Bird> birdsToShock = new Array<Bird>();
    private Array<Bird> birdsToFlyAway = new Array<Bird>();;
    private Array<Bird> birdsToMoveToNewLine = new Array<Bird>();;
    private Array<Bird> birdsToFlutter = new Array<Bird>();

    public MainStage(IGameProcessor gameProcessor){
        super(new FitViewport(ViewportUtil.VP_WIDTH, ViewportUtil.VP_HEIGHT), gameProcessor);

        SCARECROW_SIZE = (Gdx.app.getType() == Application.ApplicationType.Android ||
                          Gdx.app.getType() == Application.ApplicationType.iOS) ? 100f : 50f;
        AssetManager am = gameProcessor.getAssetManager();
        TextureAtlas atlas = am.get(AssetUtil.ANI_ATLAS, AssetUtil.TEXTURE_ATLAS);
        fastFlap = am.get(AssetUtil.SFX_BIRD_FLAP_FAST, AssetUtil.SOUND);
        deepFlap = am.get(AssetUtil.SFX_BIRD_FLAP_DEEP, AssetUtil.SOUND);
        scream = am.get(AssetUtil.SFX_BIRD_SCREAM, AssetUtil.SOUND);
        flutter = am.get(AssetUtil.SFX_BIRD_FLUTTER, AssetUtil.SOUND);

        flapSounds = new Array<Sound>();
        flapSounds.add(fastFlap);
        flapSounds.add(deepFlap);
        flapSounds.add(flutter);
        ambientBirds = am.get(AssetUtil.MSC_AMBIENT_BIRDS, AssetUtil.MUSIC);
        bgMusic = am.get(AssetUtil.MSC_BG_MUSIC, AssetUtil.MUSIC);
        bgMusic.setLooping(true);
        bgMusic.setVolume(0.2f);

        ambientBirds.setVolume(0f);
        ambientBirds.setLooping(true);
        ambientBirds.play();

        birdSitAnimations = new Array<Animation>();
        birdFlyAnimations = new Array<Animation>();
        String[] suffixes = new String[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};
        for(String suff:suffixes){
            Animation ani = new Animation(SIT_CYCLE_RATE, atlas.findRegions("bird/Bird_Sit"+suff));
            birdSitAnimations.add(ani);
            Animation flyAni = new Animation(FLY_CYCLE_RATE, atlas.findRegions("bird/Bird_Flight"+suff));
            birdFlyAnimations.add(flyAni);
        }
        shockAnimations = new Array<Animation>();
        Animation ssAni = new Animation(SHOCK_CYCLE_RATE, atlas.findRegions("bird/Bird_ShockS"));
        shockAnimations.add(ssAni);
        Animation slAni = new Animation(SHOCK_CYCLE_RATE, atlas.findRegions("bird/Bird_ShockL"));
        shockAnimations.add(slAni);

        cookedAnimations = new Array<Animation>();
        Animation cookAni = new Animation(COOKED_CYCLE_RATE, atlas.findRegions("bird/Bird_Cooked"));
        cookedAnimations.add(cookAni);

        lineMap = new ObjectMap<String, Vector2[]>();
        lineMap.put(lineNames[0], new Vector2[]{
            new Vector2(3f, 635f),
            new Vector2(766f, 566f)
        });
        lineMap.put(lineNames[1], new Vector2[]{
                new Vector2(4f, 472f),
                new Vector2(720f, 445f)
        });
        lineMap.put(lineNames[2], new Vector2[]{
                new Vector2(5f, 325f),
                new Vector2(776f, 361f)
        });
        lineMap.put(lineNames[3], new Vector2[]{
                new Vector2(847f, 566f),
                new Vector2(1257f, 682f)
        });
        lineMap.put(lineNames[4], new Vector2[]{
                new Vector2(854f, 445f),
                new Vector2(1249f, 504f)
        });
        lineMap.put(lineNames[5], new Vector2[]{
                new Vector2(853f, 365f),
                new Vector2(1254f, 371f)
        });


        birds = new Array<Bird>();
        birdDisposer = new IActorDisposer() {
            @Override
            public void dispose(Actor actor) {
                if(actor instanceof Bird){
                    birds.removeValue((Bird)actor, true);
                }
            }
        };

        surges = new Array<PowerSurge>();
        surgeDisposer = new IActorDisposer() {
            @Override
            public void dispose(Actor actor) {
                if(actor instanceof PowerSurge){
                    surges.removeValue((PowerSurge)actor, false);
                }
            }
        };


        float x, y, width, height;
        x = 0f;
        y = 0f;
        width = getWidth();
        height = getHeight();


        background = new GenericGroup(x, y, width, height, null, Color.BLUE);
        addActor(background);


        //ADD SKY BG
        //Start at 34degrees
        //max at -34degrees
        TextureRegion skyRegionA = new TextureRegion(am.get(AssetUtil.BG_SKY_A, AssetUtil.TEXTURE));
        TextureRegion skyRegionB = new TextureRegion(am.get(AssetUtil.BG_SKY_B, AssetUtil.TEXTURE));
        TextureRegion skyRegionC = new TextureRegion(am.get(AssetUtil.BG_SKY_C, AssetUtil.TEXTURE));
        TextureRegion skyRegionD = new TextureRegion(am.get(AssetUtil.BG_SKY_D, AssetUtil.TEXTURE));

        //AHHH MAGIC NUMBERS
        float skyX = -860f;
        float skyY = -2640f;

        float topY = skyRegionA.getRegionHeight();
        float rightX = skyRegionA.getRegionWidth();

        hugeAssSky = new GenericGroup(skyX, skyY, skyRegionA.getRegionWidth()*2,
                                      skyRegionA.getRegionHeight()*2, null, Color.MAGENTA);
        GenericActor hugeAssSkyA = new GenericActor(0f, topY, skyRegionA.getRegionWidth(),
                                                    skyRegionA.getRegionHeight(), skyRegionA, Color.PURPLE);
        GenericActor hugeAssSkyB = new GenericActor(rightX, topY, skyRegionB.getRegionWidth(),
                                                    skyRegionB.getRegionHeight(), skyRegionB, Color.PURPLE);
        GenericActor hugeAssSkyC = new GenericActor(rightX, 0f, skyRegionC.getRegionWidth(),
                                                    skyRegionC.getRegionHeight(), skyRegionC, Color.PURPLE);
        GenericActor hugeAssSkyD = new GenericActor(0f, 0f, skyRegionD.getRegionWidth(),
                                                    skyRegionD.getRegionHeight(), skyRegionD, Color.PURPLE);
        hugeAssSky.addActor(hugeAssSkyA);
        hugeAssSky.addActor(hugeAssSkyB);
        hugeAssSky.addActor(hugeAssSkyC);
        hugeAssSky.addActor(hugeAssSkyD);
        hugeAssSky.setRotation(BASE_SKY_ROTATION);
        hugeAssSky.setRotationSpeed(CLOUD_ROTATION_RATE);
        background.addActor(hugeAssSky);

        //ADD Clouds
        ActorDecorator d = new ActorDecorator() {
            @Override
            public void applyAdjustment(Actor actor, float v) {
                GenericActor ga = ((GenericActor)actor);
                if(ga.getX()+ga.getWidth() < 0){
                    ga.setPosition(ga.getX() + ga.getWidth()*2, 0f);
                }
            }
        };
        Array<TextureAtlas.AtlasRegion> clouds = atlas.findRegions("bg/Clouds");
        Array<GenericActor> cloudActors = new Array<GenericActor>();
        for(int i=clouds.size -1; i >= 0;i--){
            TextureAtlas.AtlasRegion region = clouds.get(i);
            GenericActor cloud = new GenericActor(0f, 0f, region.getRegionWidth(), region.getRegionHeight(), region, Color.WHITE);
            GenericActor cloud2 = new GenericActor(region.getRegionWidth(), 0f, region.getRegionWidth(), region.getRegionHeight(), region, Color.WHITE);
            cloud.velocity.x = 20f/(float)-(i+1);
            cloud2.velocity.x = 20f/(float)-(i+1);
            cloud.addDecorator(d);
            cloud2.addDecorator(d);
            cloudActors.add(cloud);
            cloudActors.add(cloud2);
            background.addActor(cloud);
            background.addActor(cloud2);
        }

        //Add Background flourishings (planes etc.)
        Label.LabelStyle style = new Label.LabelStyle(am.get(AssetUtil.REXLIA_16, AssetUtil.BITMAP_FONT), Color.BLACK);
        birdCountLabel = new Label(getBirdCountString(), style);
        birdCountLabel.setPosition(POLE_CENTER_X - (birdCountLabel.getWidth()/2), POLE_CENTER_Y);
        birdCountLabel.setVisible(false);
        background.addActor(birdCountLabel);


        //Add Scene

        Animation tree1Ani = new Animation(TREE_CYCLE_RATE*3, atlas.findRegions("bg/Tree_A"));
        AnimatedActor tree1 = new AnimatedActor(-220f, -154, 500f, 500f, tree1Ani, 0f);
        background.addActor(tree1);

        Animation tree2Ani = new Animation(TREE_CYCLE_RATE * 3, atlas.findRegions("bg/Tree_B"));
        AnimatedActor tree2 = new AnimatedActor(890f, -65f, 600f, 400f, tree2Ani, 0f);
        background.addActor(tree2);

        TextureRegion bgTexture = new TextureRegion(am.get(AssetUtil.BG_BASE, AssetUtil.TEXTURE));
        GenericActor treeScene = new GenericActor(0f, 0f, width, height, bgTexture, Color.GRAY);
        background.addActor(treeScene);



        //Add Pole and Lines
        TextureRegion leftTopRegion = new TextureRegion(am.get(AssetUtil.BG_LINE_LT, AssetUtil.TEXTURE));
        GenericActor ltLine = new GenericActor(x, y, width, height, leftTopRegion, Color.BLACK);
        background.addActor(ltLine);
        TextureRegion leftMidRegion = new TextureRegion(am.get(AssetUtil.BG_LINE_LM, AssetUtil.TEXTURE));
        GenericActor lmLine = new GenericActor(x, y, width, height, leftMidRegion, Color.BLACK);
        background.addActor(lmLine);
        TextureRegion leftBottomRegion = new TextureRegion(am.get(AssetUtil.BG_LINE_LB, AssetUtil.TEXTURE));
        GenericActor lbLine = new GenericActor(x, y, width, height, leftBottomRegion, Color.BLACK);
        background.addActor(lbLine);
        TextureRegion rightTopRegion = new TextureRegion(am.get(AssetUtil.BG_LINE_RT, AssetUtil.TEXTURE));
        GenericActor rtLine = new GenericActor(x, y, width, height, rightTopRegion, Color.BLACK);
        background.addActor(rtLine);
        TextureRegion rightMidRegion = new TextureRegion(am.get(AssetUtil.BG_LINE_RM, AssetUtil.TEXTURE));
        GenericActor rmLine = new GenericActor(x, y, width, height, rightMidRegion, Color.BLACK);
        background.addActor(rmLine);
        TextureRegion rightBottomRegion = new TextureRegion(am.get(AssetUtil.BG_LINE_RB, AssetUtil.TEXTURE));
        GenericActor rbLine = new GenericActor(x, y, width, height, rightBottomRegion, Color.BLACK);
        background.addActor(rbLine);
        TextureRegion poleRegion = new TextureRegion(am.get(AssetUtil.BG_POLE, AssetUtil.TEXTURE));
        GenericActor pole = new GenericActor(x+2f, y, width, height, poleRegion, Color.BLACK);
        background.addActor(pole);


        Animation chillToShockAni = new Animation(CTRL_BOX_CYCLE_RATE, atlas.findRegions("electric/Control_Box_Up"));
        Animation shockToChillAni = new Animation(CTRL_BOX_CYCLE_RATE, atlas.findRegions("electric/Control_Box"));
        controlBox = new AnimatedActor(POLE_CENTER_X - (CTRL_BOX_SIZE*.8f), 70f, CTRL_BOX_SIZE, CTRL_BOX_SIZE, shockToChillAni, 0f);
        controlBox.addStateAnimation("TOSHOCK", chillToShockAni);
        controlBox.setIsLooping(false);
        background.addActor(controlBox);

        Animation currentAni = new Animation(SHOCK_CYCLE_RATE, atlas.findRegions("electric/Current"));
        boxSpark = new AnimatedActor(controlBox.getX()+30f, controlBox.getY() + 85f, 40f, 25f, currentAni, 0f);
        boxSpark.setRotation(90f);
        boxSpark.setVisible(false);
        background.addActor(boxSpark);



        TextureRegion scareRegion = atlas.findRegion("scarecrow/Ring", 3);
        scareCrow = new ScareCrow(SCARECROW_SIZE, SCARECROW_SIZE, SCARECROW_SIZE, SCARECROW_SIZE, scareRegion, SCARECROW_SIZE);
        addActor(scareCrow);
        birdLifeIcons = new Array<Icon>();
    }

    private boolean isChillMode(){
        return gameMode.equals(GM_CHILL);
    }

    private String getBirdCountString(){
        String result = "";
        if(birds != null){
            int size = birds.size;
            if(size < 10){
                result += "0";
            }

            if(size < 100){
                result += "0";
            }

            result += size;
        }
        return result;
    }

    private void addLife(int position){
        TextureRegion iconRegion = new TextureRegion(gameProcessor.getAssetManager().get(AssetUtil.LIFE_ICON, AssetUtil.TEXTURE));
        TextureRegion deadRegion = new TextureRegion(gameProcessor.getAssetManager().get(AssetUtil.LIFE_ICON_USED, AssetUtil.TEXTURE));
        Icon lifeIcon = new Icon(6f+((float)position * (ICON_SIZE*0.7f)), 720f - ICON_SIZE, ICON_SIZE, ICON_SIZE, iconRegion, Color.BLACK );
        lifeIcon.setDeadRegion(deadRegion);
        addActor(lifeIcon);
        birdLifeIcons.add(lifeIcon);

    }

    private void adjustScareCrow(int screenX, int screenY){
        if(lastPosition == null){
            lastPosition = new Vector2(scareCrow.getX(), scareCrow.getY());
        }else{
            lastPosition.set(scareCrow.getX(), scareCrow.getY());
        }
        Vector2 stagePos = screenToStageCoordinates(new Vector2(screenX, screenY));
        float yOffset =  (Gdx.app.getType() == Application.ApplicationType.Android ||
                          Gdx.app.getType() == Application.ApplicationType.iOS) ? 30f : 0f;
        scareCrow.setPosition(stagePos.x-(scareCrow.getWidth()/2), stagePos.y-(scareCrow.getHeight()/2) + yOffset);
        hasMoved = true;
    }

    private void placeMarker(int screenX, int screenY){
        Vector2 stagePos = screenToStageCoordinates(new Vector2(screenX, screenY));
    }

    private void checkControlBox(int screenX, int screenY){
        Vector2 stagePos = screenToStageCoordinates(new Vector2(screenX, screenY));
        if(controlBox.collider.contains(stagePos)){
            flipGameMode();
        }
    }

    private void flipGameMode(){
        if(isChillMode()){
            gameMode = GM_SHOCK;
            Reset();
            birdCountLabel.setVisible(true);
            controlBox.setState("TOSHOCK", true);
            boxSpark.addAction(Actions.delay(0.5f, Actions.visible(true)));
        }else{
            Reset();
            gameMode = GM_CHILL;
            clearLifeCounter();
            bgMusic.stop();
            birdCountLabel.setVisible(false);
            controlBox.setState("DEFAULT", true);
            boxSpark.setVisible(false);

        }
    }

    private float getYFromX(int Xpos, float slope, float b ){
        return slope * Xpos + b;
    }

    private int getRandomXInRange(Vector2 pos1, Vector2 pos2){
        return rand.nextInt((int) pos2.x - (int) pos1.x) + (int)pos1.x;
    }

    private float getSlopeOfRange(Vector2 start, Vector2 end){
        float yDiff = end.y - start.y;
        float xDiff = end.x - start.x;
        return yDiff/xDiff;
    }

    private Vector2 getPointOnLine(Vector2 start, Vector2 end, int x){
        float slope = getSlopeOfRange(start, end);
        //y = slope*x + b;
        //b = y - slope*x
        float yIntercept = start.y - (slope*start.x);
        float yPoint = getYFromX(x, slope, yIntercept) - 10;
        return new Vector2(x, yPoint);
    }

    private Vector2 getBirdLinePosition(Vector2 start, Vector2 end){
        int xPoint = getRandomXInRange(start, end);
        return getPointOnLine(start, end, xPoint);
    }

    private Vector2 getBirdStartPosition(boolean isLeftFacing){
        //Always start at least at 400f
        float yPos = (rand.nextFloat() * MAX_SPAWN_Y) + MIN_SPAWN_Y;
        float xPos = isLeftFacing ? RIGHT_SPAWN_X : LEFT_SPAWN_X;
        return new Vector2(xPos, yPos);
    }

    private void clearLifeCounter() {
        for (Icon i:birdLifeIcons){
            i.remove();
        }
        birdLifeIcons.clear();
    }

    private void Reset(){
        BIRD_LIVES = 3;
        deadBirds = 0;
        lifeSpawn = 0;
        gameOver = false;
        loseCondition = false;
        clearLifeCounter();
        for (int i = 0; i < BIRD_LIVES; i++) {
            addLife(i);
        }
        bgMusic.stop();
        bgMusic.play();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if(!isChillMode()){
            birdCountLabel.setText(getBirdCountString());
        }

        //**
        //**
        //ADJUST AMBIENCE
        //**
        float vol = birds.size == 0 ? 0f : birds.size > 50f ? 1f : ((float)birds.size)/50f;
        ambientBirds.setVolume(vol);

        //**
        //**
        //PROCESS SCARECROW POSITION
        //**
        if(hasMoved){
            Vector2 curPos = new Vector2(scareCrow.getX(), scareCrow.getY());
            float x = ( curPos.x - lastPosition.x )/delta;
            float y = ( curPos.y - lastPosition.y )/delta;
            if(scarecrowVelocity == null){
                scarecrowVelocity = new Vector2(x, y);
            }else{
                scarecrowVelocity.set(x, y);
            }
            hasMoved = false;
        }else if(lastPosition != null){
            lastPosition.set(0f, 0f);
        }


        //**
        //**
        //GENERATE SURGES
        //**
        surgeSpawnTimeElapsed+= delta;
        if (!isChillMode() && surgeSpawnTimeElapsed >= SURGE_SPAWN_INTERVAL && !loseCondition && !gameOver){
            surgeSpawnTimeElapsed = 0f;

            TextureAtlas atlas = gameProcessor.getAssetManager().get(AssetUtil.ANI_ATLAS, AssetUtil.TEXTURE_ATLAS);
            Animation surgeAni = new Animation(1f/6f, atlas.findRegions("electric/Current"));
            Animation startAnimation = new Animation(1f/6f, atlas.findRegions("electric/Spark"));
            int powerLineTarget = rand.nextInt(lineNames.length);
            Vector2[] powerline = lineMap.get(lineNames[powerLineTarget]);
            final PowerSurge surge;
            float duration = 2f;
            if (powerLineTarget < 3){
                Vector2 target = powerline[0];
                int offset = powerLineTarget == 1 ? 60 : powerLineTarget == 2 ? 20 : 30;
                Vector2 startPos = getPointOnLine(powerline[0], powerline[1], (int)powerline[1].x + offset);
                duration = 3f;
                surge = new PowerSurge(startPos.x, startPos.y, 25f, 25f, startAnimation , target);
            }
            else {
                Vector2 target = powerline[1];
                Vector2 startPos = getPointOnLine(powerline[0], powerline[1], (int)powerline[0].x - 20);
                surge = new PowerSurge(startPos.x, startPos.y, 25f, 25f, startAnimation, target);

            }
            surge.lineIndex = powerLineTarget;
            surge.addStateAnimation("SURGING", surgeAni);
            surge.setDisposer(surgeDisposer);
            SequenceAction seq = Actions.sequence(
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            surge.setState("SURGING");
                            surge.isWarning = false;
                        }
                    }),
                    Actions.moveTo(surge.getTargetPosition().x, surge.getTargetPosition().y - 5f, duration),
                    Actions.run(new Runnable() {
                          @Override
                          public void run() {
                              surge.setIsRemovable(true);
                          }
                      }));
            surge.addAction(Actions.delay(3f, seq));

            addActor(surge);
            surges.add(surge);
        }


        //**
        //**
        //GENERATE BIRDS
        //**
        birdSpawnTimeElapsed+= delta;
        if(birdSpawnTimeElapsed >= BIRD_SPAWN_INTERVAL && !gameOver){
            birdSpawnTimeElapsed = 0f;

            if(!isChillMode()){
                lifeSpawn += 1;

                if (lifeSpawn >= LIFE_SPAWN_INTERVAL){
                    lifeSpawn = 0;
                    BIRD_LIVES += 1;
                    addLife(BIRD_LIVES -1);
                }
            }

            SequenceAction sequence = Actions.sequence();

            int lineTarget = rand.nextInt(lineNames.length);
            Vector2[] line = lineMap.get(lineNames[lineTarget]);
            Vector2 birdPoint = getBirdLinePosition(line[0], line[1]);
            float yOffset = (lineTarget == 3 || lineTarget == 4) ? + 10f : 0f;
            birdPoint.y += yOffset;
            MoveToAction birdIncomingAction = Actions.moveTo(birdPoint.x, birdPoint.y, BIRD_FLY_IN_TIME);
            birdIncomingAction.setInterpolation(Interpolation.exp10Out);
            sequence.addAction(birdIncomingAction);

            //Our Bird flying animations are faced as follows:
            //A <-- 0    F <-- 5
            //B --> 1    G --> 6
            //C --> 2    H --> 7
            //D --> 3    I <-- 8
            //E --> 4    J <-- 9
            int i = rand.nextInt(birdSitAnimations.size);
            boolean isLeftFacing = i == 0 || i == 5 || i == 8 || i == 9;
            Vector2 startPos = getBirdStartPosition(isLeftFacing);
            int shockIndex = (i == 3 || i == 6 || i == 9) ? 1 : 0;

            final Bird bird = new Bird(startPos.x, startPos.y, BIRD_SIZE, BIRD_SIZE, birdFlyAnimations.get(i), birdPoint);
            bird.isLeftFacing = isLeftFacing;
            bird.lineIndex = lineTarget;
            bird.addStateAnimation("SITTING", birdSitAnimations.get(i));
            bird.addStateAnimation("FLYING", birdFlyAnimations.get(i));
            bird.addStateAnimation("SHOCKED", shockAnimations.get(shockIndex));
            bird.addStateAnimation("COOKED", cookedAnimations.get(0));
            sequence.addAction(Actions.run(new Runnable() {
                @Override
                public void run() {
                    bird.setState("SITTING", true);
                }
            }));


            bird.setDisposer(birdDisposer);
            birds.add(bird);
            addActor(bird);

            if(!bird.isFlapping){
                deepFlap.play(0.1f);
            }
            bird.addAction(sequence);

        }

        if (loseCondition && birds.size == 0){
            gameOver = true;
        }

        //**
        //**
        //PROCESS BIRD INTERACTIONS
        //**

        birdsToFlyAway.clear();
        birdsToMoveToNewLine.clear();
        birdsToShock.clear();
        birdsToFlutter.clear();

        for(Bird bird:birds) {
            final Bird b = bird;

            if(b.isLeaving || b.isShocked){
                continue;
            }
            //**
            //Find any Surge Collisions
            boolean surgeCollide = false;
            //Only collide with a surge if the bird is sitting on
            //  the line and not shocked
            //if(!b.isShocked && !b.isFlapping && !b.isLeaving){
            if(!b.isFlapping){
                for (PowerSurge surge:surges){
                    //Only allow collisions with birds on the current line
                    if (!surge.isWarning && surge.lineIndex == bird.lineIndex && bird.collider.overlaps(surge.collider)){
//                        birdsToShock.add(bird);
                        surgeCollide = true;
                        break;
                    }
                }
            }

            if (!isChillMode() && (loseCondition || gameOver || bird.isOutOfRange())){
                birdsToFlyAway.add(bird);
            }else if (!isChillMode() && surgeCollide){
                birdsToShock.add(bird);
            } else if(!bird.isShocked && bird.collider.overlaps(scareCrow.collider) &&
                      (scareCrow.getCenterY()) < bird.getCenterY()) {

                if (scarecrowVelocity != null) {
                    float maxVelocity = Math.max(Math.abs(scarecrowVelocity.x), Math.abs(scarecrowVelocity.y));

                    if(maxVelocity > 1000f){
                        //scale = 6f;
                        birdsToFlyAway.add(bird);
                    }else if(maxVelocity > 500f){
                        birdsToMoveToNewLine.add(bird);
                    }else if(maxVelocity < 300f){
                        birdsToFlutter.add(bird);
                    }
                }else{
                    birdsToFlutter.add(bird);
                }
            }
        }

        boolean isInitialLose = false;
        for(Bird bird:birdsToShock){
            final Bird b = bird;
            bird.clearActions();
            bird.setState("SHOCKED", true);
            scream.play(0.4f);
            bird.isShocked = true;
            MoveToAction away = Actions.moveTo(bird.getX(), 0f, 1.5f);
            bird.addAction(Actions.delay(0.8f, Actions.sequence(Actions.run(new Runnable() {
                @Override
                public void run() {
                    b.setState("COOKED", true);
                }
            }),away, Actions.run(new Runnable() {
                @Override
                public void run() {
                    b.setIsRemovable(true);
                }
            }))));
            birdLifeIcons.get(deadBirds).markUsed();
            birdLifeIcons.get(deadBirds).flipTextureRegion(true, true);
            if(deadBirds < BIRD_LIVES -1) {
                deadBirds += 1;
            }else{
                loseCondition = true;
                finalFlockSize = birds.size;
                isInitialLose = true;
            }
        }

        if(isInitialLose){
            birdsToFlyAway.addAll(birdsToFlutter);
            birdsToFlyAway.addAll(birdsToMoveToNewLine);
            birdsToFlutter.clear();
            birdsToMoveToNewLine.clear();
        }

        for(Bird bird:birdsToFlyAway){
            final Bird b = bird;
            b.clearActions();
            b.isLeaving = true;
            float xTarget = b.isLeftFacing ? 20f : 1260f;
            MoveToAction moveAction = Actions.moveTo(xTarget, 1000f, 0.8f);
            b.addAction(Actions.sequence(moveAction, Actions.run(new Runnable() {
                @Override
                public void run() {
                    b.setIsRemovable(true);
                }
            })));
        }

        for(Bird bird:birdsToMoveToNewLine){
            final Bird b = bird;
            Vector2 birdPos = new Vector2(bird.getX(), bird.getY());
            Vector2 scPos = new Vector2(scareCrow.getX(), scareCrow.getY());
            int randomFloat = new Random().nextInt(40) - 20;
            Vector2 distance = new Vector2((float) randomFloat, birdPos.y - scPos.y);

            int newTarget = bird.lineIndex;
            while(newTarget == bird.lineIndex){
                newTarget = rand.nextInt(lineNames.length);
            }
            //newTarget = (newTarget + 1)%lineNames.length;
            bird.lineIndex = newTarget;   //fix bird line switch can't collide with surge
            Vector2[] line = lineMap.get(lineNames[newTarget]);
            Vector2 birdPoint = getBirdLinePosition(line[0], line[1]);
            birdPoint.y += (newTarget == 3 || newTarget == 4) ? + 10f : 0f;
            bird.setTargetPosition(birdPoint);

            final int newTargetIndex = newTarget;
            distance = distance.add(birdPos);
            bird.clearActions();
            bird.setState("FLYING", true);
            if(!bird.isFlapping){
                int flapSound = rand.nextInt(flapSounds.size);
                flapSounds.get(flapSound).play(0.2f);
                bird.isFlapping = true;
            }
            MoveToAction away = Actions.moveTo(distance.x, distance.y, 0.5f);
            away.setInterpolation(Interpolation.exp5In);
            MoveToAction back = Actions.moveTo(bird.getTargetPosition().x, bird.getTargetPosition().y, 1.5f);
            back.setInterpolation(Interpolation.exp5Out);
            bird.addAction(Actions.sequence(away, back, Actions.run(new Runnable() {
                @Override
                public void run() {
                    b.setState("SITTING", true);
                    b.isFlapping = false;
                    b.lineIndex = newTargetIndex;
                }
            })));
        }

        for(Bird bird:birdsToFlutter){
            final Bird b = bird;
            Vector2 birdPos = new Vector2(bird.getX(), bird.getY());
            Vector2 scPos = new Vector2(scareCrow.getX(), scareCrow.getY());
            int randomFloat = new Random().nextInt(40) - 20;
            Vector2 distance = new Vector2((float) randomFloat, birdPos.y - scPos.y);
            int newTarget = bird.lineIndex;

            final int newTargetIndex = newTarget;
            distance = distance.add(birdPos);
            bird.clearActions();
            bird.setState("FLYING", true);
            if(!bird.isFlapping){
                int flapSound = rand.nextInt(flapSounds.size);
                flapSounds.get(flapSound).play(0.2f);
                //fastFlap.play(0.05f);
                //deepFlap.play(0.4f);
                bird.isFlapping = true;
            }
            MoveToAction away = Actions.moveTo(distance.x, distance.y, 0.5f);
            MoveToAction back = Actions.moveTo(bird.getTargetPosition().x, bird.getTargetPosition().y, 0.8f);
            back.setInterpolation(Interpolation.exp5Out);
            bird.addAction(Actions.sequence(away, back, Actions.run(new Runnable() {
                @Override
                public void run() {
                    b.setState("SITTING", true);
                    b.isFlapping = false;
                    b.lineIndex = newTargetIndex;
                    Gdx.app.log("BIRD LANDING", "bird on new line: " + birds.contains(b, true));
                }
            })));
        }


    }


    @Override
    public void draw() {
        super.draw();
    }

    @Override
    public boolean keyDown(int keyCode) {
        if(Input.Keys.SPACE == keyCode && gameOver){
            Reset();
        }
        return super.keyDown(keyCode);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        adjustScareCrow(screenX, screenY);
        placeMarker(screenX, screenY);

        checkControlBox(screenX, screenY);
        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        adjustScareCrow(screenX, screenY);
        return super.touchDragged(screenX, screenY, pointer);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        adjustScareCrow(screenX, screenY);
        return super.mouseMoved(screenX, screenY);
    }

    @Override
    public boolean scrolled(int amount) {
        ((OrthographicCamera)getCamera()).zoom += (float)amount/4f;
        return super.scrolled(amount);
    }
}
