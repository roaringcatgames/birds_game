package com.roaringcatgames.ludumdare.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.roaringcatgames.ludumdare.LudumDare31Game;
import com.roaringcatgames.ludumdare.ViewportUtil;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = ViewportUtil.VP_WIDTH;
        config.height = ViewportUtil.VP_HEIGHT;
        config.title = "BIRDS!";
		new LwjglApplication(new LudumDare31Game(false), config);
	}
}
