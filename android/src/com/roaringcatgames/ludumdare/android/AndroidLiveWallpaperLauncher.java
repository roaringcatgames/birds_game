package com.roaringcatgames.ludumdare.android;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;
import com.roaringcatgames.ludumdare.LudumDare31Game;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 12/23/14
 * Time: 5:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class AndroidLiveWallpaperLauncher extends AndroidLiveWallpaperService {




    @Override
    public void onCreateApplication() {
        super.onCreateApplication();

        final AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useCompass = false;
        config.useWakelock = false;
        config.useAccelerometer = false;
        config.getTouchEventsForLiveWallpaper = true;

        final ApplicationListener listener = new LudumDare31Game(false);
        initialize(listener, config);
    }


}
