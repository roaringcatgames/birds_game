package com.roaringcatgames.ludumdare.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.roaringcatgames.ludumdare.LudumDare31Game;
import com.roaringcatgames.ludumdare.ViewportUtil;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(ViewportUtil.VP_WIDTH, ViewportUtil.VP_HEIGHT);
        }

        @Override
        public ApplicationListener getApplicationListener () {
                return new LudumDare31Game(false);
        }
}